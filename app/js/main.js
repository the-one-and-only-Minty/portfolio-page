// GLOBAL VARIABLES
var windowWidth = $(document).width();
var homeWidth;
var expandSum1;
var expandSum2;
var expandResult;
var totalProjects = $(".project").length;
var stage = "step1";

// END

$("document").ready(function() {

// watches out for resize - and re-runs calculations of project column size depending 
$(window).resize(function() {
  var windowWidthNew = $(document).width();
  if ( windowWidthNew == windowWidth ){
  }
  else {
    windowWidth = windowWidthNew;
    if( stage == "step2"){
      expandCalc("yes");
    }
    else if ( stage == "step3" ){
      project();
    }
  }
});
var totalProjects = $(".project").length;

// PROJECTS SHOW - Sets stage 2
$(".reveal").click(function() {
	  stage = "step2";
  expandCalc("no");
});


// Calculating project column size at stage 2

function expandCalc(resized) {
  homeWidth = 80;
  if ( stage == "step2" ) {
  		expandSum1 = windowWidth - homeWidth;
	  	expandSum2 = expandSum1 / totalProjects;
	  	expandResult = expandSum2 / windowWidth * 100;
  	$(".home").css("width", homeWidth);
  	$(".project").css("width", expandResult + "%");
  }

// conditional added with parameter so that the home menu doesnt get run on the resize event - only on the click.
  if (resized == "no") {
    $(".content").fadeOut("slow", function() {
      $(".icon").fadeIn("slow", function() {});
    });
  }
}
// PROJECTS HIDE
$(".conceal").click(function() {
  $(".home").css("width", 100 + "%");
  $(".project").css("width", 0).removeClass("active");
  $(".icon").fadeOut("slow", function() {
    $(".content").fadeIn("slow", function() {});
  });
  stage = "step1";
});

//SET NUMBER - used for identifying projects in the calculation functions
$(".project").each(function(index, value) {
  $(this).attr("number", index + 1);
});

function project(){
  var numberValue = $(".clicked-project").attr("number");
  var counter;
  var className;

  for (counter = 1; counter <= 4; ++counter) {
    var remainingProjects = totalProjects - 1;
    var minimisedProject = 50;
    var expandedProject = windowWidth - (minimisedProject * remainingProjects + homeWidth);
    if (counter == numberValue) {
      $(".project[number='" + counter + "']").css({
        "width": expandedProject,
        "cursor":"auto"})
      .addClass("active");
    } else {
      $(".project[number='" + counter + "']").css("width", minimisedProject)
      .removeClass("active");
    }
  }
}

//Sets stage 3 - adds/removes class that is needed to identify selected project in the second calculation function
$(".project").click(function() {
  stage = "step3";
  $(".project").removeClass("clicked-project");
  $(this).addClass("clicked-project");
  project();
});


});